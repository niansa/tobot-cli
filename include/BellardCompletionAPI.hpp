#include <nlohmann/json.hpp>
#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/beast/ssl.hpp>
#include <chrono>
#include <functional>



class BellardCompletionAPI {
    boost::asio::ip::tcp::resolver resolver;
    boost::asio::ssl::context ctx{boost::asio::ssl::context::tlsv12_client};
    std::chrono::minutes timeout = std::chrono::minutes(2);
    boost::beast::http::request_parser<boost::beast::http::string_body> header_parser;
    boost::asio::io_context& ex;

public:
    BellardCompletionAPI(boost::asio::io_context& ex) : ex(ex), resolver(ex) {}
    static constexpr size_t bufSize = 16;

    void complete(const std::string& prompt, char delim, std::function<void (const std::string&)> finally, std::function<void (const boost::beast::error_code&, std::string_view)> on_error = nullptr);
};
